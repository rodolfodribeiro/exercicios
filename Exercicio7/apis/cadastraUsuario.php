<?php
include_once('../common/include.php');

$usuario = json_decode(file_get_contents("php://input"));
//Valida se todos os campos estão preenchidos
//Caso não esteja, retorna para o usuário o erro
if(!$usuario->nome) {
    sendResponse(400, [], 'Favor informar o Nome.');  
}else if(!$usuario->sobrenome) {
    sendResponse(400, [] , 'Favor informar o Sobrenome.');  
}else if(!$usuario->email) {
    sendResponse(400, [] , 'Favor informar o Email.');        
}else if(!$usuario->telefone) {
    sendResponse(400, [] , 'Favor informar o Telefone');  
}else {

    $arquivo = '../base/registros.txt';
    $dados = $usuario->nome . '|' . $usuario->sobrenome . '|' . $usuario->email . '|' . $usuario->telefone . "\n";
    // Salva as informações no arquivo, caso ele exista, acrescenta ao invés de sobrescrever
    $ret = file_put_contents($arquivo, $dados, FILE_APPEND | LOCK_EX);
    if($ret === false) {
        sendResponse(500, [], 'Erro ao fazer o cadastro!');
    }
    else {
        sendResponse(200, $result , 'Usuario cadastrado com sucesso.');
    }


}
?>
