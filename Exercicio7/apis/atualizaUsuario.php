<?php
include_once('../common/include.php');

$usuario = json_decode(file_get_contents("php://input"));
//Valida se todos os campos estão preenchidos
//Caso não esteja, retorna para o usuário o erro
if(!$usuario->nome) {
    sendResponse(400, [], 'Favor informar o Nome.');  
}else if(!$usuario->sobrenome) {
    sendResponse(400, [] , 'Favor informar o Sobrenome.');  
}else if(!$usuario->email) {
    sendResponse(400, [] , 'Favor informar o Email.');        
}else if(!$usuario->telefone) {
    sendResponse(400, [] , 'Favor informar o Telefone');  
}else {

    $arr = array();
    $atualizado = false;
    $conn=getConnection();
    if(!$conn){
        sendResponse(500,$conn,'Nao existe arquivo com os dados');
    }else{
        $arquivo = $conn;
        $dados = $usuario->nome . '|' . $usuario->sobrenome . '|' . $usuario->email . '|' . $usuario->telefone . "\n";

        $arq = fopen($arquivo, 'r');
        while( ( $linha = fgets( $arq ) ) !== false )
        {
            $item = explode( '|', $linha );  
            if ($item[2] == $usuario->email) { 
                array_push($arr,$dados);
                $atualizado = true;
            }
            else {
                array_push($arr,$linha);
            }
        }
        if($atualizado) {
            sendResponse(200, [], 'Usuario atualizado com sucesso.');
        }
        else {
            sendResponse(500, [], 'Usuario nao encontrado!');
        }

        file_put_contents($arquivo, $arr);
    }

}
?>