<?php
include_once('../common/include.php');
$conn=getConnection();

if(!$conn){
    sendResponse(500,$conn,'Nao existe arquivo com os dados');
}else{
    $arq = fopen($conn, 'r');
    $users=array();
    while( ( $linha = fgets( $arq ) ) !== false )
    {
        $item = explode( '|', $linha );
        $user=array(
            "nome" =>  $item[0],
            "sobrenome" => $item[1],
            "email" => $item[2],
            "telefone" => $item[3],
        );
        array_push($users,$user);
    }
    fclose( $arq );
    $numResult = count($users);
    if($numResult >= 1) {
        sendResponse(200,$users,'Lista de usuarios.');
    }
    else {
        sendResponse(404,[],'Nao exitem usuarios cadastrados.');
    }
    
}
?>
