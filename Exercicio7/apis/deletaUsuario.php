<?php
include_once('../common/include.php');
$email = $_GET['email'];
if ($email == "") {
    sendResponse(500, [], 'Favor informar o email do usuario a ser deletado.');
}
else {
    $deletado = false;
    $conn=getConnection();
    if(!$conn) {
        sendResponse(500,$conn,'Nao existe arquivo com os dados');
    }else {
        $arquivo = $conn;
        if (file_exists($arquivo)) {
            $arr = array();
            $arq = fopen($arquivo, 'r');
            while( ( $linha = fgets( $arq ) ) !== false )
            {
                $item = explode( '|', $linha );  
                if ($item[2] != $email) { 
                  array_push($arr,$linha);
                }
                else {
                    sendResponse(200, [], 'Usuario deletado com sucesso.');
                    $deletado = true;
                }
            }
            if(!$deletado){
                sendResponse(500, [], 'Email nao encontrado!');
            }
            file_put_contents($arquivo, $arr);
        }
    }
}
       
?>