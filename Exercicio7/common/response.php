<?php
/**
 * Funcao para retornar as mensagens para o usuario
 */
function sendResponse($resp_code,$data,$message){
    echo json_encode(array('code'=>$resp_code,'message'=>$message,'data'=>$data));
}
?>