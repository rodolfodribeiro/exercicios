<?php

$extFiles = array( 'mp4', 'jpeg', 'mov' );
//Verifica os arquivos que estão dentro da pasta arquivos
if ( $handle = opendir('arquivos') ) {
	//percorre por todos arquivos da pasta
    while ( $files = readdir( $handle ) ) {
    	//pega as extensões dos arquivos e armazena em um array
        $ext = strtolower( pathinfo( $files, PATHINFO_EXTENSION) );
        if( in_array( $ext, $extFiles ) ){
        	$arrExt[] = $ext;	
        } 
    }
    closedir($handle);
    //faz ordenação do array e imprime na tela
    asort($arrExt);
    foreach ($arrExt as $arrOut){
		echo "$arrOut<br>";
	}
}    

?>
