<?php
$location = array("Brasil" => "Brasília", "Argentina" => "Buenos Aires", "Colômbia" => "Bogotá", "Chile" => "Santiago", "Paraguai" => "Assunção","Peru" => "Lima" );
//Faz a ordenação
asort($location);
foreach ($location as $country => $capital) {
    // Verifica a ultima letra do país para definir o artigo a ser utilizado
    $lastChar = substr($country, -1);
    if ($lastChar == "a") {
        echo "A capital da $country é $capital.<br>";
    } else {
        echo "A capital do $country é $capital.<br>";
    }
}

?>