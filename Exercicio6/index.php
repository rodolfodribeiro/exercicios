<?php

$paises=array("Argentina","Brasil","Chile","Colômbia","Paraguai","Uruguai","Outros");
 
class SelectField{

  private $name;
  private $value;
 
  public function getName(){
     return $this->name;
  }

  //O nome a ser setado será o name html do campo Select
  public function setName($name){
     $this->name = $name;
  }

  public function getValue(){
     return $this->value;
  }
 
  //O value será a propriedade value do campo select
  public function setValue($value){
     $this->value = $value;
   }
 
  //Cria as opcoes do Select a serem apresentadas na tela
  private function criaOptions($value){
     foreach($value as $val){
        echo "<option value=\"$val\">" .$val. "</option>\n";
      }
  }
 
  //Função para criar o campo Select com as opções
  public function criaSelect(){
     echo "<select name=\"".$this->getName()."\">\n";
     $this->criaOptions($this->getValue());
     echo "</select>" ;
  }
}
 
?>

<!DOCTYPE html>
<html lang="pt-br">  
  <body> 
    <form method="post" action="">
      <p>Nome:<br/>
      <input type="text" name="nome" size="40" />  </p>
      <p>Sobrenome:<br/>
      <input type="text" name="sobrenome" size="40" /></p>
      <p>Email:<br/>
      <input type="email" name="email" size="40" /></p>
      <p>País:<br/>
      <?php
        $paisSelect = new SelectField();
        //Informa o nome do campo
        $paisSelect->setName('pais');
        //Informa as opçoess do campo
        $paisSelect->setValue($paises);
        //Cria o campo com os dados informados acima
        $paisSelect->criaSelect();
      ?>
      </p>
      <p>Login:<br/>
      <input name="login" type="text" required size="40" /></p>
      <p>Senha:<br/>
      <input name="senha" type="password" required size="40" /></p>
      <input type="submit" name="submit" value="Cadastrar" />
    </form>
  </body>
</html>