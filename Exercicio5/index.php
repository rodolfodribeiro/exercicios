<?php
//Favor criar um arquivo chamado teste.xml na raiz para ser transformado em .csv
$fileXml='teste.xml';

    if (file_exists($fileXml)) 
    {
       $xml = simplexml_load_file($fileXml);
       $fp = fopen('teste.csv', 'w');
       criaCsv($xml, $fp);
       fclose($fp);
    }

    function criaCsv($xml,$fp)
    {
    	//Percorre por todo o xml
        foreach ($xml->children() as $item) 
        {
        	//Verifica se tem mais filho, senão, chama a funcao novamente de forma recursiva
        	$hasChild = (count($item->children()) > 0)?true:false;

	        if( !$hasChild ) {
	           $arr = array($item->getName(),$item); 
	           fputcsv($fp, $arr ,',','"');
	        }
	        else {
	         criaCsv($item, $fp);
	        }
	    }
    } 	
?>