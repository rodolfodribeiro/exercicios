
<?php
header("Expires: 0");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$arquivo = 'registros.txt';

if (file_exists($arquivo)) {

  $arq = fopen($arquivo, 'r');
  $erroCadastro = false;
  while( ( $linha = fgets( $arq ) ) !== false )
  {
    $item = explode( '|', $linha );
       
    if ($item[2] == $_POST['email']) {
      $erroCadastro = true;
      echo('Email já cadastrado!<br>');
    }
    else if ($item[4] == $_POST['login']) {
      $erroCadastro = true;
      echo('Login já cadastrado!<br>');
    }
  }
  fclose( $arq );
  if(!$erroCadastro) {
    salvar($arquivo);
  }
}
//Cria um novo arquivo e salva
else
{
    salvar($arquivo);
}

function salvar($arquivoTxt)
{
  //Criptografa a senha
  $pass = md5($_POST['senha']);
  $data = $_POST['nome'] . '|' . $_POST['sobrenome'] . '|' . $_POST['email'] . '|' . $_POST['phone'] . '|' . $_POST['login'] . '|' . $pass . "\n";
  // Salva as informações no arquivo, caso ele exista, acrescenta ao invés de sobrescrever
  $ret = file_put_contents($arquivoTxt, $data, FILE_APPEND | LOCK_EX);
  if($ret === false) {
    echo "Erro para salvar o arquivo";
  }
  else {
    echo "Cadastro salvo com sucesso!";
  }
}

?>